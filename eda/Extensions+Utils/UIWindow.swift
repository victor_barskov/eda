//
//  UIWindow.swift
//  emop
//
//  Created by Victor Barskov on 19/07/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation
import UIKit

public extension UIWindow {
    class var mainWindow: UIWindow {
        return UIApplication.shared.windows.first!
    }
}
