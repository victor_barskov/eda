//
//  CGFloat.swift
//  emop
//
//  Created by Victor Barskov on 02/11/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    static func getRandomFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
