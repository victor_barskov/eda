//
//  UIDevice.swift
//  emop
//
//  Created by Victor Barskov on 19/07/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation

public enum Model : String {
    case simulator = "simulator/sandbox",
    iPod1          = "iPod 1",
    iPod2          = "iPod 2",
    iPod3          = "iPod 3",
    iPod4          = "iPod 4",
    iPod5          = "iPod 5",
    iPad2          = "iPad 2",
    iPad3          = "iPad 3",
    iPad4          = "iPad 4",
    iPhone4        = "iPhone 4",
    iPhone4S       = "iPhone 4S",
    iPhone5        = "iPhone 5",
    iPhone5S       = "iPhone 5S",
    iPhone5C       = "iPhone 5C",
    iPadMini1      = "iPad Mini 1",
    iPadMini2      = "iPad Mini 2",
    iPadMini3      = "iPad Mini 3",
    iPadAir1       = "iPad Air 1",
    iPadAir2       = "iPad Air 2",
    iPhone6        = "iPhone 6",
    iPhone6plus    = "iPhone 6 Plus",
    iPhone6S       = "iPhone 6S",
    iPhone6Splus   = "iPhone 6S Plus",
    unrecognized   = "?unrecognized?"
}

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafeMutablePointer(to: &systemInfo.machine) {
            ptr in String(cString: UnsafeRawPointer(ptr).assumingMemoryBound(to: CChar.self))
        }
        var modelMap : [ String : Model ] = [
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad2,5"   : .iPadMini1,
            "iPad2,6"   : .iPadMini1,
            "iPad2,7"   : .iPadMini1,
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPad4,1"   : .iPadAir1,
            "iPad4,2"   : .iPadAir2,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPhone7,1" : .iPhone6plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6Splus
        ]
        
        if let model = modelMap[String(modelCode)] {
            return model
        }
        return Model.unrecognized
    }
}

extension UIDevice {
    //    var iPhone: Bool {
    //        return UIDevice().userInterfaceIdiom == .phone
    //    }
    enum ScreenType: String {
        case iPhone4
        case iPhone5
        case iPhone678
        case iPhone678Plus
        case iPhoneX
        case iPad2
        case iPadRetina
        case unknown
    }
    var screenType: ScreenType {
        //        guard iPhone else { return .unknown }
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone678
        case 2208:
            return .iPhone678Plus
        case 2436:
            return .iPhoneX
        case 1024:
            return .iPad2
        case 2048:
            return .iPadRetina
        default:
            return .unknown
        }
    }
    // Use UIDevice.current.screenType...
}
//extension UIDevice {
//    var iPhone: Bool {
//        return UIDevice().userInterfaceIdiom == .phone
//    }
//    enum ScreenType: String {
//        case iPhone4
//        case iPhone5
//        case iPhone6
//        case iPhone6Plus
//        case unknown
//    }
//    var screenType: ScreenType {
//        guard iPhone else { return .unknown }
//        switch UIScreen.main.nativeBounds.height {
//        case 960:
//            return .iPhone4
//        case 1136:
//            return .iPhone5
//        case 1334:
//            return .iPhone6
//        case 2208:
//            return .iPhone6Plus
//        default:
//            return .unknown
//        }
//        // Use UIDevice.current.screenType...
//    }
//}

