//
//  NSObject.swift
//  emop
//
//  Created by Victor Barskov on 19/07/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation

public extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
