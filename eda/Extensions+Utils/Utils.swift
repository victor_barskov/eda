//
//  Utils.swift
//  ApService
//
//  Created by Victor Barskov on 14.01.16.
//  Copyright © 2016 Victor Barskov. All rights reserved.

import UIKit
import EventKit


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

public enum AvenirFontType {
    case Light, Book, Heavy, Medium
}

public struct CurrentFont {
    
    let currentFont: UIFont!
    
    init(fontSize: CGFloat, fontType: AvenirFontType) {
        
        switch fontType {
        case .Light:
            currentFont = UIFont(name: "Avenir-Light", size: fontSize)
            break
        case .Heavy:
            currentFont = UIFont(name: "Avenir-Heavy", size: fontSize)
            break
        case .Medium:
            currentFont = UIFont(name: "Avenir-Medium", size: fontSize)
            break
        case .Book:
            currentFont = UIFont(name: "Avenir-Book", size: fontSize)
            break
        }
    }
}

public func isReleaseVersion() -> Bool {
    
    if let bundleID = Bundle.main.bundleIdentifier {
        if bundleID == "world.cleaner.emop.release" {
            return true
        } else if bundleID == "world.cleaner.emop.debug" {
            return false
        } else {
            return false
        }
    } else {
        return false
    }
}

public func showHidenByTaps (viewController: UIViewController, selector: String) {
    
    let tap = UITapGestureRecognizer()
    
    // DOUBLE TAP
    tap.numberOfTapsRequired = 9
    tap.numberOfTouchesRequired = 3
    tap.addTarget(viewController, action: Selector((selector)))
    viewController.view.isUserInteractionEnabled = true
    viewController.view.addGestureRecognizer(tap)
    
}

public func setAllPhoneCodes (viewController: UIViewController, selector: String) {
    
    let tap = UITapGestureRecognizer()
    
    // DOUBLE TAP
    tap.numberOfTapsRequired = 6
    tap.numberOfTouchesRequired = 2
    tap.addTarget(viewController, action: Selector((selector)))
    viewController.view.isUserInteractionEnabled = true
    viewController.view.addGestureRecognizer(tap)
    
}

// MARK: - IBDesignable -

@IBDesignable class RoundRectView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor = UIColor.black
    @IBInspectable var borderWidth: CGFloat = 0.5
    @IBInspectable var shadowColor: UIColor = UIColor.black
    
    private var customBackgroundColor = UIColor.white
    
    override var backgroundColor: UIColor?{
        didSet {
            customBackgroundColor = backgroundColor!
            super.backgroundColor = UIColor.clear
        }
    }
    
    func setup() {
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 5.0
        layer.shadowOpacity = 0.5
        super.backgroundColor = UIColor.clear
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func draw(_ rect: CGRect) {
        
        customBackgroundColor.setFill()
        
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius ).fill()
        
        let borderRect = bounds.insetBy(dx: borderWidth/2, dy: borderWidth/2)
        let borderPath = UIBezierPath(roundedRect: borderRect, cornerRadius: cornerRadius - borderWidth/2)
        borderColor.setStroke()
        shadowColor.setFill()
        borderPath.lineWidth = borderWidth
        borderPath.stroke()
        
        // whatever else you need drawn
    }
}

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        
        textRect.origin.x += leftPadding
        
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            
            leftViewMode = UITextFieldViewMode.unlessEditing
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
}

@IBDesignable
class UISwitchCustom: UISwitch {
    @IBInspectable var OffTint: UIColor? {
        didSet {
            self.tintColor = OffTint
            self.layer.cornerRadius = 16
            self.backgroundColor = OffTint
        }
    }
}

public func randomString(length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}





func stringDateFromUnix (unixDate: Float) -> String {
    
    let timeInterval = TimeInterval(exactly: Float(unixDate))
    let date = Date(timeIntervalSince1970: timeInterval!)
    
    // initialize the date formatter and set the style
    let date_formatter = DateFormatter()
    date_formatter.timeStyle = .none
    date_formatter.dateStyle = .long
    
    return date_formatter.string(from:date)
    
}

public func stringTimeFromUnix (unixDate: Float) -> String {
    
    let timeInterval = TimeInterval(exactly: Float(unixDate))
    let date = Date(timeIntervalSince1970: timeInterval!)
    
    // initialize the time formatter and set the style
    let time_formatter = DateFormatter()
    time_formatter.timeStyle = .short
    time_formatter.dateStyle = .none
    
    return time_formatter.string(from:date)
    
}

// MARK: - TASKS -

func beginBackgroundTask() -> UIBackgroundTaskIdentifier {
    return UIApplication.shared.beginBackgroundTask(expirationHandler: {})
}

func endBackgroundTask(_ taskID: UIBackgroundTaskIdentifier) {
    UIApplication.shared.endBackgroundTask(taskID)
}

// MARK: - ALERTING -

public func getRandomColor() -> UIColor {
    let randomRed:CGFloat = CGFloat(drand48())
    let randomGreen:CGFloat = CGFloat(drand48())
    let randomBlue:CGFloat = CGFloat(drand48())
    return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
}

public func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / image.size.width
    let heightRatio = targetSize.height / image.size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}

public func drawFillCircle(view: UIView, alpha: CGFloat, radius: Int, strokeColor: UIColor, fillColor: UIColor, lineWidth: CGFloat) {
    
    let circlePath = UIBezierPath(arcCenter: CGPoint(x: radius,y: radius), radius: view.bounds.size.width/2 - lineWidth, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = circlePath.cgPath
    
    //change the fill color
    shapeLayer.fillColor = fillColor.cgColor
    //you can change the stroke color
    shapeLayer.strokeColor = strokeColor.cgColor
    //you can change the line width
    shapeLayer.lineWidth = lineWidth
    
    view.alpha = alpha
    view.layer.addSublayer(shapeLayer)
    
}

public func drawCircle(view: UIView,  radius: Int, circleRad: CGFloat, color: UIColor, lineWidth: CGFloat) {
    
    let circlePath = UIBezierPath(arcCenter: CGPoint(x: radius,y: radius), radius: circleRad - lineWidth, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = circlePath.cgPath
    
    //change the fill color
    shapeLayer.fillColor = UIColor.clear.cgColor
    //you can change the stroke color
    shapeLayer.strokeColor = color.cgColor
    //you can change the line width
    shapeLayer.lineWidth = lineWidth
    
    
    view.layer.addSublayer(shapeLayer)
    
}

public func colorsWithHalfOpacity(_ colors: [CGColor]) -> [CGColor] {
    return colors.map({ $0.copy(alpha: $0.alpha * 0.5)! })
}

// MARK: - MISC -

public struct Platform {
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
    }
    
}


public func canOpenURL(_ string: String?) -> Bool {
    guard let urlString = string else {return false}
    guard let url = URL(string: urlString) else {return false}
    if !UIApplication.shared.canOpenURL(url) {return false}
    
    //
    let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
    let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
    return predicate.evaluate(with: string)
}


public func addEventToCalendar(title: String, description: String?, startDate: NSDate, endDate: NSDate, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
    
    let eventStore = EKEventStore()
    
    eventStore.requestAccess(to: .event, completion: { (granted, error) in
        if (granted) && (error == nil) {
            let event = EKEvent(eventStore: eventStore)
            event.title = title
            event.startDate = startDate as Date
            event.endDate = endDate as Date
            event.notes = description
            event.calendar = eventStore.defaultCalendarForNewEvents
            do {
                try eventStore.save(event, span: .thisEvent)
            } catch let e as NSError {
                completion?(false, e)
                return
            }
            completion?(true, nil)
        } else {
            completion?(false, error as NSError?)
        }
    })
}

public func toDecimal (_ number: String) -> String {
    
    let formatter: NumberFormatter = NumberFormatter()
    formatter.numberStyle = NumberFormatter.Style.decimal
    guard let intu = Double(number) else {return "---"}
    let numberFromInt = NSNumber(value: intu as Double)
    guard let stringFromNumber = formatter.string(from: numberFromInt) else {return "---"}
    return stringFromNumber
}

public func toDecimal_ru (_ number: String) -> String {
    
    let formatter: NumberFormatter = NumberFormatter()
    formatter.numberStyle = NumberFormatter.Style.currency
    guard let intu = Double(number) else {return "---"}
    let numberFromInt = NSNumber(value: intu as Double)
    guard let stringFromNumber = formatter.string(from: numberFromInt) else {return "---"}
    return stringFromNumber
}


// Getting local time zone helper

public func ltzOffsetInHours() -> Int { return NSTimeZone.local.secondsFromGMT() / 3600 }

// Make our time equal to UTC

public func makeUTC (_ dateToChange: Date?) ->  Date {
    
    let calendar = Calendar.autoupdatingCurrent
    let newDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.hour, value: ltzOffsetInHours(), to: dateToChange!, options: [])
    
    return newDate!
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}


func isCoordsShared() -> Bool {
    
    if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
        CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)
    {
        return true
    } else {
        
        let locationAuthStatus = CLLocationManager.authorizationStatus()
        if locationAuthStatus == .denied || locationAuthStatus == .restricted {
            return false
        } else {
            return true
        }
    }
}


final class ViewDecorator {
    
    static let shared = ViewDecorator()
    private init() {}
    
    func addShadow(_ view : UIView?, shadow: CGFloat, color: CGColor?)
    {
        view?.layer.masksToBounds = false
        view?.layer.shadowOffset = CGSize(width: 0, height: 3)
        view?.layer.shadowOpacity = 0.9
        view?.layer.shadowRadius = shadow
        view?.layer.shadowColor = color
    }
    func roundCorners(_ view: UIView, corners:UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
        view.layer.masksToBounds = true
    }
    
}




