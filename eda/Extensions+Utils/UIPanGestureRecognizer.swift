//
//  UIPanGestureRecognizer.swift
//  emop
//
//  Created by Victor Barskov on 19/07/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation
import UIKit

public enum Direction {
    case up
    case down
    case left
    case right
}

//MARK: - UIPanGestureRecognizer
public extension UIPanGestureRecognizer {
    
    var direction: Direction? {
        let velocity = self.velocity(in: view)
        let isVertical = fabs(velocity.y) > fabs(velocity.x)
        
        switch (isVertical, velocity.x, velocity.y) {
        case (true, _, let y) where y < 0: return .up
        case (true, _, let y) where y > 0: return .down
        case (false, let x, _) where x > 0: return .right
        case (false, let x, _) where x < 0: return .left
        default: return nil
        }
    }
}
