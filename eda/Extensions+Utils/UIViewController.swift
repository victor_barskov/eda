//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    func setData(data: Any?) -> Any {
        return data ?? false
    }
    
    func setIdentifier(identifier: String) -> String {
        return identifier
    }
    
}

public extension UIViewController {
    func removeView(with tag: Int?) {
        if let tag = tag {
            for view in self.view.subviews {
                if view.tag == tag {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
}

public extension UIViewController {
    
    func hideKeyboardWhenTapped() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: Email validation
    
    func isValidEmail(_ testStr: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
    
    func isValidPhoneNumber(_ phoneStr: String) -> Bool {
        
        let phoneRegEx = "^\\d{7,}"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: phoneStr)
        
    }
    
    func isNumeric(_ numStr: String) -> Bool {
        
        let numericRegEx = "^[0-9]+$"
        let numTest = NSPredicate(format:"SELF MATCHES %@", numericRegEx)
        return numTest.evaluate(with: numStr)
        
    }
    
    // Getting local time zone helper
    
    func ltzOffsetInHours() -> Int { return NSTimeZone.local.secondsFromGMT() / 3600 }
    
    // Make our time equal to UTC
    
    func makeUTC (_ dateToChange: Date?) ->  Date? {
        
        let calendar = Calendar.autoupdatingCurrent
        let newDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.hour, value: ltzOffsetInHours(), to: dateToChange!, options: [])
        
        return newDate
    }
}

extension UIViewController {
    static func loadFromXib(bundle: Bundle? = nil, overriddenXib: String? = nil) -> Self {
        return self.init(nibName: overriddenXib ?? String(describing: self), bundle: bundle)
    }
}
