//
//  Presenter.swift
//  emop
//
//  Created by Victor Barskov on 01/02/2018.
//  Copyright © 2018 wifiasyougo. All rights reserved.
//

import Foundation
import UIKit

protocol PresentingViewController {
    func presentViewController<T>(vcType: T, storyBoard: String, vcId: String, data: Any?, transitionDelegate: Bool?, centered: Bool?)
}

extension PresentingViewController where Self: UIViewController {
    
    func presentViewController<T>(vcType: T, storyBoard: String, vcId: String, data: Any?, transitionDelegate: Bool?, centered: Bool?) {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: vcId) as? T else { return }
        if transitionDelegate ?? false {
            let vContr = viewController as? UIViewController ?? UIViewController()
            TransitioningDelegate.centered = centered ?? true
            vContr.transitioningDelegate = TransitioningDelegate
            self.present(vContr, animated: true, completion: nil)
        } else {
            let navigationController = UINavigationController(rootViewController: viewController as? UIViewController ?? UIViewController())
            self.navigationController?.present(navigationController, animated: true, completion: nil)
        }
    }
}

let TransitionController = CustomTransitionController()
let TransitioningDelegate = CustomTransitioningDelegate()

class CustomTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {
    
    var centered = true
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        TransitionController.centered = centered
        return TransitionController
    }
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        TransitionController.centered = centered
        return TransitionController
    }
    
}

/// MARK: Transition Controller

class CustomTransitionController: NSObject, UIViewControllerAnimatedTransitioning {
    
    var presentingController: UIViewController?
    var centered = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let container = transitionContext.containerView
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let reversed = container.viewWithTag(0xabc) != nil
        
        if reversed {
            
            let blurredView = container.viewWithTag(0xabc)
            let snapshotView = container.viewWithTag(0xabcd)
            
            UIView.animate(withDuration: 0.3,
                           animations: {
                            fromVC.view.alpha = 0
                            blurredView!.alpha = 0
            },
                           completion: { finished in
                            fromVC.view.removeFromSuperview()
                            blurredView!.removeFromSuperview()
                            snapshotView!.removeFromSuperview()
                            transitionContext.completeTransition(true)
                            self.presentingController = nil
            })
            
        } else {
            
            var snapshot = fromVC.view.snapshotImage()
            let snapshotView = UIImageView(image: snapshot)
            snapshotView.tag = 0xabcd
            
            let tintColor = UIColor(white: 0.11, alpha: 0.8)
            snapshot = snapshot?.applyBlur(withRadius: 5, tintColor:tintColor, saturationDeltaFactor:1.8, maskImage:nil)
            
            let blurredView = UIImageView(image: snapshot)
            blurredView.tag = 0xabc
            blurredView.isUserInteractionEnabled = true
            
            blurredView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomTransitionController.tapBack(recognizer:))))
            
            snapshotView.frame = container.bounds
            blurredView.frame = container.bounds
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            label.text = "Close".localized(with_comment: "")
            label.font = UIFont.systemFont(ofSize: 6.0)
            label.textAlignment = .center
            label.textColor = UIColor.white
            
            let size = toVC.preferredContentSize // The preferredContentSize should be set in the toVC viewDidLoad method
            
            if size.width > 0 && size.height > 0 {
                
                toVC.view.frame.size.height = toVC.preferredContentSize.height
                toVC.view.frame.size.width = toVC.preferredContentSize.width
                toVC.view.center = container.center
                
//                if centered {
//                    toVC.view.frame.size.width = transitionContext.finalFrame(for: toVC).size.width - 40
//                    toVC.view.center = container.center
//                } else {
//                    toVC.view.frame.size.width = transitionContext.finalFrame(for: toVC).size.width
//                }
                
                //                switch Int(UIScreen.main.bounds.height) {
                //                case 480:
                //                    label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 23)
                //                    toVC.view.frame.origin.y = 30
                //                case 568:
                //                    if (size.height > 200) {
                //                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 40)
                //                        toVC.view.frame.origin.y = toVC.view.frame.size.height / 2
                //                    } else {
                //                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 70)
                //                        toVC.view.frame.origin.y = 80
                //                    }
                //                default:
                //                    if (size.height > 200) {
                //                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 55)
                //                        toVC.view.frame.origin.y = 65
                //                    } else {
                //                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 140)
                //                        toVC.view.frame.origin.y = 150
                //                    }
                //                }
            } else {
                toVC.view.frame = transitionContext.finalFrame(for: toVC)
            }
            
            //            blurredView.addSubview(label)
            container.addSubview(snapshotView)
            container.addSubview(blurredView)
            container.addSubview(toVC.view)
            
            toVC.view.alpha = 0
            blurredView.alpha = 0
            
            self.presentingController = fromVC
            
            UIView.animate(withDuration: 0.3,
                           animations: {
                            toVC.view.alpha = 1
                            blurredView.alpha = 1
            },
                           completion: { finished in
                            transitionContext.completeTransition(true)
            })
        }
    }
    
    @objc func tapBack(recognizer: UIGestureRecognizer) {
        if recognizer.state == .ended {
            presentingController?.dismiss(animated: true, completion: nil)
        }
    }
}

