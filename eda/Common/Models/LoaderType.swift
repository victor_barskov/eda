//
//  LoaderType.swift
//  eda
//
//  Created by Victor Barskov on 26/09/2018.
//  Copyright © 2018 barvi. All rights reserved.
//

import Foundation

enum LoaderType: String {
    case sd, king, nuke
}
