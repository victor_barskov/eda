//
//  Place.swift
//  eda
//
//  Created by Victor Barskov on 26/09/2018.
//  Copyright © 2018 barvi. All rights reserved.
//

import Foundation

class Place {
    
    let picURL: String
    let name: String
    let descr: String
    
    init (json: JSON?) {
        if let puri = json?["picture"]["uri"].string {
            if let ind = puri.index(of: "-") {
                let substr = puri.prefix(upTo: ind)
                self.picURL = basePICURL+substr+basePictureSize
            } else {
               self.picURL = "https://eda.yandex/images/places/c741/c741a77ebc1a29c504fb950692c6345c-100x75.jpg"
            }
        } else {
            self.picURL = json?["picture"]["uri"].string ?? "https://eda.yandex/images/places/c741/c741a77ebc1a29c504fb950692c6345c-100x75.jpg"
        }
        self.name = json?["name"].string ?? "..."
        self.descr = json?["footerDescription"].string ?? "..."
    }
}
