//
//  RequestManager.swift
//  eda
//
//  Created by Victor Barskov on 25/09/2018.
//  Copyright © 2018 barvi. All rights reserved.
//

import Foundation
import Alamofire

class RequestManger: NSObject {
    
    class func getPlaces (completion: @escaping ([Place]?, Error?)->Void) {
        
        var places = [Place]()
        Alamofire.request(baseAPIURL,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers: nil).responseJSON { response in
            
            switch response.result {
                
            case .success(let object):
                
                let json = JSON(object)
                #if DEBUG
                #endif
                let placesJSON = json["payload"]["foundPlaces"]
                if placesJSON.count > 0 {
                    for place in placesJSON {
                        let place = Place(json: place.1["place"])
                        places.append(place)
                    }
                    completion(places, nil)
                }
                
            case .failure:
                if let error = response.error {
                   completion(nil, error)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }

}
