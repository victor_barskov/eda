//
//  Consts.swift
//  eda
//
//  Created by Victor Barskov on 25/09/2018.
//  Copyright © 2018 barvi. All rights reserved.
//

import Foundation

let baseAPIURL = "https://eda.yandex/api/v2/catalog?latitude=55.762885&longitude=37.597360"
let basePICURL = "https://eda.yandex"
let basePictureSize = "-100x75.jpg"
let edaTableCellIdentifier = "cell"
