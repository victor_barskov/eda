//
//  ViewController.swift
//  eda
//
//  Created by Victor Barskov on 25/09/2018.
//  Copyright © 2018 barvi. All rights reserved.
//

import UIKit
import Kingfisher
import SDWebImage
import Nuke

class ViewController: UIViewController {

    var places = [Place]()
    var loader: LoaderType = .king
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setup()
        getPlaces()
    }
    
    private func setup() {
        self.title = "Eda"
        let sdButton   = UIBarButtonItem(title: LoaderType.sd.rawValue, style: .plain, target: self, action: #selector(setLoader(sender:)))
        sdButton.tag = 1
        let kingButton   = UIBarButtonItem(title: LoaderType.king.rawValue, style: .plain, target: self, action: #selector(setLoader(sender:)))
        kingButton.tag = 2
        let nukeButton   = UIBarButtonItem(title: LoaderType.nuke.rawValue, style: .plain, target: self, action: #selector(setLoader(sender:)))
        nukeButton.tag = 3
        
        let clearCacheButton   = UIBarButtonItem(title: "clear", style: .plain, target: self, action: #selector(clearCache))
        
        navigationItem.rightBarButtonItems = [sdButton, kingButton, nukeButton]
        navigationItem.leftBarButtonItems = [clearCacheButton]
    }
    
}

extension ViewController {
    
    private func getPlaces() {
        places.removeAll()
        table.reloadData()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        RequestManger.getPlaces {[weak self] (places, error) in
            MBProgressHUD.hideAllHUDs(for: self?.view, animated: true)
            if error == nil {
                if let places = places {
                    self?.places = places
                    self?.table.reloadData()
                }
            } else {
                self?.systemAlert(title: "Alert", message: "Error occured...")
            }
        }
    }
    
    private func systemAlert(title: String, message: String) {
        let alertController = UIAlertController (title: title, message: message , preferredStyle: .alert)
        let tryAction = UIAlertAction(title: "Try again", style: .default) { (_) -> Void in
            self.getPlaces()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(tryAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc private func setLoader(sender: UIBarButtonItem) {
        switch sender.tag {
        case 1:
            loader = .sd
            getPlaces()
            break
        case 2:
            loader = .king
            getPlaces()
            break
        case 3:
            loader = .nuke
            getPlaces()
            break
        default:
            break
        }
    }
    
    @objc private func clearCache() {
        KingfisherManager.shared.cache.clearMemoryCache()
        KingfisherManager.shared.cache.clearDiskCache()
        KingfisherManager.shared.cache.cleanExpiredDiskCache()
        SDImageCache.shared().clearMemory()
        ImageCache.shared.removeAll()
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: edaTableCellIdentifier) as? EdaTableViewCell {
            cell.configure(place: places[indexPath.row], loader: loader)
            return cell
        } else {
            return UITableViewCell(frame: CGRect.zero)
        }
    }
    
}

