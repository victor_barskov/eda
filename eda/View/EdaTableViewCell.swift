//
//  EdaTableViewCell.swift
//  eda
//
//  Created by Victor Barskov on 26/09/2018.
//  Copyright © 2018 barvi. All rights reserved.
//

import UIKit
import Kingfisher
import SDWebImage
import Nuke

class EdaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descr: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(place: Place?, loader: LoaderType) {
        guard let place = place else {
            return
        }
        guard let url = URL(string: place.picURL) else {
            return
        }
        switch loader {
        case .sd:
            pic.sd_setImage(with: url, placeholderImage: nil)
            break
        case .king:
            pic.kf.setImage(with: url)
            break
        case .nuke:
            Nuke.loadImage(with: url, into: pic)
            break
        }

        self.name.text = place.name
        self.descr.text = place.descr
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
